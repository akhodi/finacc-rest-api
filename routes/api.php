<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('profile', 'AuthController@getAuthenticatedUser');
    Route::get('logout', 'AuthController@logout');

    Route::resource('financial-accounts', 'FinancialAccountController');
    Route::resource('financial-transactions', 'FinancialTransactionController');

    Route::get('transaction-summary/daily', 'TransactionSummaryController@daily');
    Route::get('transaction-summary/monthly', 'TransactionSummaryController@monthly');
});
