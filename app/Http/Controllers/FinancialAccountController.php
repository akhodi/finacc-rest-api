<?php

namespace App\Http\Controllers;

use App\FinancialAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;

use Illuminate\Support\Facades\Validator;
use JWTAuth;

class FinancialAccountController extends BaseController
{
    public function index(Request $request)
    {
        $financial_accounts = FinancialAccount::with(['user'])->where('user_id', JWTAuth::user()->id);

        // find datas by keywords
        if ($request->query('_find')) {
            $financial_accounts = $financial_accounts->where('name', 'ilike', '%' . $request->query('_find') . '%');
        }

        // filter by account name
        if ($request->query('name')) {
            $financial_accounts = $financial_accounts->where('name', 'ilike', '%' . $request->query('name') . '%');
        }

        // sort data
        if ($request->query('_sort') && $request->query('_order')) {
            $financial_accounts = $financial_accounts->orderBy($request->query('_sort'), $request->query('_order'));
        } else {
            $financial_accounts = $financial_accounts->orderBy('id', 'DESC');
        }

        $data = $financial_accounts->paginate($request->query('_limit'))->toArray();
        return $this->sendResponse($data, 'Financial Accounts retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|min:3',
            '_type' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->messages(), [], 401);
        }

        if (!empty($input['_type']) && !in_array($input['_type'], array('cash', 'bank', 'ewallet'))) {
            return $this->sendError('Account type must be in options cash, bank or ewallet', [], 401);
        }

        $input['user_id'] = JWTAuth::user()->id;
        $financial_account = FinancialAccount::create($input);

        return $this->sendResponse($financial_account->toArray(), 'Financial Account created successfully.');
    }

    public function show(FinancialAccount $financial_account)
    {
        if (is_null($financial_account))  {
            return $this->sendError('Financial Account not found.');
        }

        $data = FinancialAccount::with(['user'])->find($financial_account->id);

        return $this->sendResponse($data->toArray(), 'Financial Account retrieved successfully.');
    }

    public function update(Request $request, FinancialAccount $financial_account)
    {
        if ($financial_account->user_id != JWTAuth::user()->id) {
            return $this->sendError('You don\'t have permission', [], 401);
        }

        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|min:3',
            '_type' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->messages(), [], 401);
        }

        if (!empty($input['_type']) && !in_array($input['_type'], array('cash', 'bank', 'ewallet'))) {
            return $this->sendError('Account type must be in options cash, bank or ewallet', [], 401);
        }

        $financial_account->name = $input['name'];
        $financial_account->_type = $input['_type'];
        $financial_account->description = $input['description'];
        $financial_account->save();

        return $this->sendResponse($financial_account, 'Financial Account updated successfully.');
    }

    public function destroy(FinancialAccount $financial_account)
    {
        $financial_account->delete();

        return $this->sendResponse($financial_account->toArray(), 'Financial Account deleted successfully.');
    }
}
