<?php

namespace App\Http\Controllers;

use App\FinancialTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;

use JWTAuth;
use DB;

class TransactionSummaryController extends BaseController
{
    public function daily()
    {

        $query = DB::table('financial_transactions')
            ->where('user_id', JWTAuth::user()->id)
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('SUM(amount) as amount'))
            ->groupBy('date')
            ->get();

        return $this->sendResponse($query->toArray(), 'Transaction Summary retrieved successfully.');
    }

    public function monthly()
    {
        $query = DB::table('financial_transactions')
            ->where('user_id', JWTAuth::user()->id)
            ->select(DB::raw('EXTRACT(YEAR FROM created_at) as year'), DB::raw('EXTRACT(MONTH FROM created_at) as month'), DB::raw('SUM(amount) as amount'))
            ->groupby('year', 'month')
            ->get();

        return $this->sendResponse($query->toArray(), 'Transaction Summary retrieved successfully.');
    }
}
