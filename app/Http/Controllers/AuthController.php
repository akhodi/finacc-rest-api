<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends BaseController
{
    public function register(Request $request)
    {
        $credentials = $request->only(
            'name',
            'username',
            'email',
            'password'
        );

        $rules = [
            'name' => 'required',
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return $this->sendError($validator->messages(), [], 401);
        }

        $name = $request->name;
        $username = $request->username;
        $email = $request->email;
        $password = $request->password;

        User::create([
            'name' => $name,
            'username' => $username,
            'email' => $email,
            'password' => Hash::make($password)
        ]);

        return $this->login($request);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return $this->sendError($validator->messages(), [], 401);
        }

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->sendError('Email or Password is invalid.', [], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->sendError('Failed to login, please try again.', [], 401);
        }

        $user = JWTAuth::user();
        $user['token'] = $token;

        return $this->sendResponse($user, 'Login retrieved successfully.');
    }

    public function getAuthenticatedUser()
    {
        return $this->sendResponse(JWTAuth::user(), 'Profile retrieved successfully.');
    }

    public function logout(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::invalidate($request->input('token'));
            return $this->sendResponse(JWTAuth::user(), 'You have successfully logged out.');
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->sendError('Failed to logout, please try again.', [], 401);
        }
    }
}
