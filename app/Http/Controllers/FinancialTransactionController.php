<?php

namespace App\Http\Controllers;

use App\FinancialTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;

use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Carbon\Carbon;

class FinancialTransactionController extends BaseController
{
    public function index(Request $request)
    {
        $financial_transactions = FinancialTransaction::with([
            'financial_account',
            'financial_account.user'
        ])->where('user_id', JWTAuth::user()->id);

        // find datas by keywords
        if ($request->query('_find')) {
            $financial_transactions = $financial_transactions->where('name', 'ilike', '%' . $request->query('_find') . '%');
        }

        // filter datas by transaction date
        if ($request->query('_date')) {
            $financial_transactions = $financial_transactions->whereBetween('created_at', [
                Carbon::parse($request->query('_date'))->startOfDay(),
                Carbon::parse($request->query('_date'))->endOfDay()
            ]);
        }

        // filter datas by financial account name
        if ($request->query('account_name')) {
            $financial_transactions = $financial_transactions->whereHas('financial_account', function($q) use ($request) {
                $q->where('name', 'ilike', '%' . $request->query('account_name') . '%');
            });
        }

        // sort data
        if ($request->query('_sort') && $request->query('_order')) {
            $financial_transactions = $financial_transactions->orderBy($request->query('_sort'), $request->query('_order'));
        } else {
            $financial_transactions = $financial_transactions->orderBy('id', 'DESC');
        }

        $data = $financial_transactions->paginate($request->query('_limit'))->toArray();
        return $this->sendResponse($data, 'Financial Transactions retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'financial_account_id' => 'required|exists:financial_accounts,id',
            'amount' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->messages(), [], 401);
        }

        if (!empty($input['in_out']) && !in_array($input['in_out'], array('in', 'out'))) {
            return $this->sendError('in_out must be in options in or out', [], 401);
        }

        $input['user_id'] = JWTAuth::user()->id;
        $financial_transaction = FinancialTransaction::create($input);

        return $this->sendResponse($financial_transaction->toArray(), 'Financial Transaction created successfully.');
    }

    public function show(FinancialTransaction $financial_transaction)
    {
        if (is_null($financial_transaction))  {
            return $this->sendError('Financial Transaction not found.');
        }

        $data = FinancialTransaction::with([
            'financial_account',
            'financial_account.user'
        ])->find($financial_transaction->id);

        return $this->sendResponse($data->toArray(), 'Financial Transaction retrieved successfully.');
    }

    public function update(Request $request, FinancialTransaction $financial_transaction)
    {
        if ($financial_transaction->user_id != JWTAuth::user()->id)  {
            return $this->sendError('You don\'t have permission', [], 401);
        }

        $input = $request->all();

        $validator = Validator::make($input, [
            'financial_account_id' => 'required|exists:financial_accounts,id',
            'amount' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->messages(), [], 401);
        }

        if (!empty($input['in_out']) && !in_array($input['in_out'], array('in', 'out'))) {
            return $this->sendError('in_out must be in options in or out', [], 401);
        }

        $financial_transaction->financial_account_id = $input['financial_account_id'];
        $financial_transaction->reference = $input['reference'];
        $financial_transaction->amount = $input['amount'];
        $financial_transaction->save();

        return $this->sendResponse($financial_transaction, 'Financial Transaction updated successfully.');
    }

    public function destroy(FinancialTransaction $financial_transaction)
    {
        $financial_transaction->delete();

        return $this->sendResponse($financial_transaction->toArray(), 'Financial Transaction deleted successfully.');
    }
}
