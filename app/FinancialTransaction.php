<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FinancialTransaction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'financial_account_id',
        'reference',
        'in_out',
        'amount',
        'user_id'
    ];

    protected $hidden = [
        'financial_account_id',
        'user_id'
    ];

    public function financial_account()
    {
        return $this->belongsTo('App\FinancialAccount');
    }
}
