## Requirements

- PHP v7.x.x
- Laravel
- MySQL or PostgreSQL

## Setup

#### Quick Setup / Re-Setup:
```
php artisan migrate
```

#### Development Environment:
```bash
composer install
```

#### Secret Key Base:
```bash
php artisan jwt:secret
```

#### Run Web Server (puma)
```
php artisan serve
```


## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
